FROM python:3

COPY ./src/ /app

WORKDIR /app
RUN pip install -r requirements.txt

EXPOSE 5000
CMD ["python", "run.py"]