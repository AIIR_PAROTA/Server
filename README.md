# Server
1. Main goal
  * Server application to communicate between client (web) application and cluster.
2. Prerequisites
  * Python
  * Flask

3. How to start backend?
  * Make sure you already have a running docker-machine (see: Frontend README)
  * Ssh into machine `docker-machine ssh aiir`
  * Go to Server directory `cd /Main-project-dir/Server`
  * Run backend docker `docker-compose up -d backend`
  * Make sure everything is running, by opening `http://<aiir-docker-machine-IP>:5000/dev/status` (or by running command `docker-compose ps`)