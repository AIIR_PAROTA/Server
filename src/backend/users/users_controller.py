from ..util.security import ts
from ..util.send_email import send_email
from .user import User, collection
from backend import db, app, bcrypt
import bson.json_util as json
from bson.objectid import ObjectId
from flask import Blueprint, request, abort, url_for, render_template
from flask.ext.api import status
from flask_login import login_user, logout_user, current_user

users = Blueprint('users', __name__, url_prefix='/users')


@users.route('', methods=['GET'])
def get_all_users():
    return json.dumps(collection.find())


@users.route('', methods=['POST'])
def create_user():
    if not request.json:
        app.logger.error('Missing user json')
        abort(status.HTTP_400_BAD_REQUEST)
    user = request.json
    existing_user = collection.find_one({'email': user['email']})
    if existing_user is None:
        app.logger.info('Sending an email')
        token = ts.dumps(user['email'], salt=app.config["EMAIL_CONFIRM_KEY"])
        confirm_url = url_for('users.confirm_account', token=token, _external=True)
        html = render_template('activate.html', confirm_url=confirm_url)
        if not send_email(user['email'], html):
            app.logger.error('Sending email failed')
            abort(status.HTTP_500_INTERNAL_SERVER_ERROR)
        app.logger.info('Email sent')

        app.logger.info('Creating user...')
        user['password'] = bcrypt.generate_password_hash(user['password'])
        user['email_confirmed'] = False
        collection.insert_one(user)
        app.logger.info('User created')
        return "", status.HTTP_201_CREATED
    app.logger.error('User already exists')
    return "", status.HTTP_409_CONFLICT


@users.route('/login', methods=['POST'])
def login():
    if not request.json:
        app.logger.error('Missing user json')
        abort(status.HTTP_400_BAD_REQUEST)
    user_info = request.json
    user = collection.find_one({'email': user_info['email']})
    if user and User.validate_login(user['password'], user_info['password']):
        user_obj = User(user['email'])
        login_user(user_obj)
        app.logger.info('User logged in')
        return "", status.HTTP_200_OK
    app.logger.error('Wrong username or password')
    return "", status.HTTP_400_BAD_REQUEST


@users.route('/session', methods=['GET'])
def get_session():
    app.logger.info("Getting session...")
    if current_user.is_authenticated:
        app.logger.info("Completed")
        return json.dumps(collection.find_one({'email': current_user.get_id()}))
    app.logger.info("Session not found")
    return "", status.HTTP_404_NOT_FOUND


@users.route('/<user_id>', methods=['GET'])
def get_user(user_id):
    return json.dumps(collection.find_one({'_id': ObjectId(user_id)}))


@users.route('/<user_id>', methods=['PUT'])
def update_user(user_id):
    if not request.json:
        app.logger.error('Missing user json')
        abort(status.HTTP_400_BAD_REQUEST)
    user_info = request.json
    for field in user_info:
        if field == "password":
            value = bcrypt.generate_password_hash(user_info['password'])
            collection.find_one_and_update({'_id': ObjectId(user_id)}, {'$set': {'password': value}})
        elif field != "_id":
            collection.find_one_and_update({'_id': ObjectId(user_id)}, {'$set': {field: user_info[field]}})
    return "", status.HTTP_200_OK


@users.route('/<user_id>', methods=['DELETE'])
def delete_user(user_id):
    collection.delete_one({'_id': ObjectId(user_id)})
    return "", status.HTTP_200_OK


@users.route('/logout', methods=['POST'])
def logout():
    logout_user()
    return "", status.HTTP_200_OK


@users.route('/confirm/<token>', methods=['GET'])
def confirm_account(token):
    try:
        app.logger.info(token)
        email = ts.loads(token, salt=app.config["EMAIL_CONFIRM_KEY"], max_age=86400)
        app.logger.info(email)
    except:
        abort(status.HTTP_404_NOT_FOUND)

    app.logger.info('Searching for user')
    user = collection.find_one({'email': email})
    if user is None:
        abort(status.HTTP_404_NOT_FOUND)

    user["email_confirmed"] = True
    collection.save(user)

    return "", status.HTTP_200_OK
