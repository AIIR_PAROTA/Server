from backend import bcrypt
from backend import db, app, login_manager

collection = db.users


@login_manager.user_loader
def load_user(email):
    u = collection.find_one({"email": email})
    if not u:
        return None
    return User(u['email'])


class User:
    def __init__(self, email):
        self.email = email

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.email

    @staticmethod
    def validate_login(password_hash, password):
        return bcrypt.check_password_hash(password_hash, password)