from flask import Flask
import logging
from logging.handlers import RotatingFileHandler
from pymongo import MongoClient

app = Flask(__name__)
app.config.from_object('config')

client = MongoClient(app.config["DB_URL"])
db = client[app.config["DB_NAME"]]

from flask_login import LoginManager
login_manager = LoginManager()
login_manager.init_app(app)

from flask_bcrypt import Bcrypt
bcrypt = Bcrypt(app)

# Register modules
from backend.dev_module.controller import dev_module
app.register_blueprint(dev_module)
from backend.jobs.jobs_controller import jobs
app.register_blueprint(jobs)
from backend.users.users_controller import users
app.register_blueprint(users)




