import bson.json_util as json
from collections import OrderedDict
from io import BytesIO
from itertools import count, product
from string import ascii_uppercase
import xlrd
import xlwt
from .. import app


def get_file_extension(seq=ascii_uppercase):
    for n in count(1):
        for s in product(seq, repeat=n):
            yield ''.join(s)


def get_all_sheet_values(sh):
    output_list = []
    fields = sh.row_values(0)
    for i in range(1, sh.nrows):
        row = sh.row_values(i)
        record = OrderedDict()
        for num, field in enumerate(fields):
            if isinstance(row[num], float) and row[num].is_integer():
                record[field.lower()] = str(int(row[num]))
            else:
                record[field.lower()] = str(row[num])
        output_list.append(record)
    return output_list


def convert_string_fields_to_lists(instance):
    for element_type in instance:
        for element in instance[element_type]:
            if "groups" in element:
                element["groups"] = [int(x.strip()) for x in element["groups"].split(';') if x]
            if "features" in element:
                element["features"] = [x.strip() for x in element["features"].split(';') if x]
            if "constraints" in element:
                element["constraints"] = [x.strip() for x in element["constraints"].split(';') if x]
            if "timeslots" in element:
                element["timeslots"] = [int(x.strip()) for x in element["timeslots"].split(';') if x]
            if "entities" in element:
                element["entities"] = [int(x.strip()) for x in element["entities"].split(';') if x]
            if element_type == "timeslots" and "id" in element:
                element["id"] = [int(x.strip()) for x in element["id"].split(';') if x]


def find_timeslot(instance, id):
    for element in instance["timeslots"]:
        if isinstance(element["id"], list):
            if id in element["id"]:
                return element
        else:
            if id == element["id"]:
                return element



def put_timeslots_to_timeconteiners(instance):
    for element in instance["timecontainers"]:
        timeslot_list = []
        for timeslot_id in element["timeslots"]:
            timeslot_list.append(find_timeslot(instance, timeslot_id))
        element["timeslots"] = timeslot_list


def split_time(instance):
    output_list = []
    for time_slot in instance["timeslots"]:
        for id in time_slot["id"]:
            new_time_slot = time_slot.copy()
            new_time_slot["id"] = id
            output_list.append(new_time_slot)
    instance["timeslots"] = output_list


def create_empty_list_field(instance,element_type,field_name):
    for element in instance[element_type]:
        element[field_name] = []


def create_empty_field(instance,element_type,field_name):
    for element in instance[element_type]:
        element[field_name] = ""


def add_relations(instance, from_element_type, to_element_type, from_name, to_name):
    for element in instance[from_element_type]:
        if not isinstance(element[from_name], str) and not isinstance(element[from_name], int):
            for element_field in element[from_name]:
                a = next(filter(lambda e: e["id"] == element_field, instance[to_element_type]))
                if isinstance(a[to_name], list):
                    a[to_name].append(element["id"])
                else:
                    a[to_name] = element["id"]
        elif element[from_name] != "":
            a = next(filter(lambda e: e["id"] == element[from_name], instance[to_element_type]))
            if isinstance(a[to_name], list):
                a[to_name].append(element["id"])
            else:
                a[to_name] = element["id"]


def add_next_and_previous(instance):
    for container in instance["timecontainers"]:
        for i, time_slot in enumerate(container["timeslots"]):
            to_change = next(x for x in instance["timeslots"] if x["id"] == time_slot)
            if i > 0:
                to_change["previous"] = container["timeslots"][i - 1]
            if i < len(container["timeslots"]) - 1:
                to_change["next"] = container["timeslots"][i + 1]


def check_ids_type(instance):
    for element_type in instance:
        for element in instance[element_type]:
            if "id" in element:
                try:
                    int(element["id"])
                except ValueError:
                    raise ValueError("Id must be an integer - " + element_type + " " + element["id"])


def check_timecontainers(instance):
    timeslots_list = []
    for container in instance["timecontainers"]:
        for id in container["timeslots"]:
            if id in timeslots_list:
                raise Exception("Time slot should be only used once " + id)
            timeslots_list.append(id)


def excel_to_json(file):
    wb = xlrd.open_workbook(file_contents=file.read(), encoding_override="utf-8")
    output_instance = OrderedDict()

    for sheet_name in (wb.sheet_names()):
        sheet = wb.sheet_by_name(sheet_name)
        output_instance[sheet_name.lower()] = get_all_sheet_values(sheet)

    convert_string_fields_to_lists(output_instance)

    string_to_int(output_instance, "entities", "id")
    string_to_int(output_instance, "events", "id")
    string_to_int(output_instance, "events", "timeslot")
    string_to_int(output_instance, "events", "location")
    string_to_int(output_instance, "groups", "id")
    string_to_int(output_instance, "locations", "id")
    string_to_int(output_instance, "timecontainers", "id")

    split_time(output_instance)
    check_ids_type(output_instance)
    check_timecontainers(output_instance)
    add_next_and_previous(output_instance)

    create_empty_list_field(output_instance, "groups", "entities")
    create_empty_list_field(output_instance, "timeslots", "timecontainers")
    create_empty_list_field(output_instance, "entities", "events")
    create_empty_list_field(output_instance, "locations", "event")
    create_empty_field(output_instance, "timeslots", "event")
    create_empty_field(output_instance, "timeslots", "timecontainer")

    add_relations(output_instance, "entities", "groups", "groups", "entities")
    add_relations(output_instance, "events", "entities", "entities", "events")
    add_relations(output_instance, "events", "locations", "location", "event")
    add_relations(output_instance, "events", "timeslots", "timeslot", "event")
    add_relations(output_instance, "timecontainers", "timeslots", "timeslots", "timecontainer")

    put_timeslots_to_timeconteiners(output_instance)

    j = json.dumps(output_instance)
    return j


def print_sheet(name, data, output_file):
    sheet = output_file.add_sheet(name.capitalize())

    column_count = 0
    for a in data[name][0]:
        if len(str(a)) * 256 > sheet.col(column_count).width and len(str(a)) * 256 <  100 * 256:
            sheet.col(column_count).width = len(str(a)) * 256
        if a != "previous" and a != "next":
            sheet.write(0, column_count, a)
            column_count += 1

    if name == "timeslots":
        sheet.write(0, column_count, "previous")
        sheet.write(0, column_count + 1, "next")

    for i, record in enumerate(data[name]):
        j = 0
        for key in record.keys():
            if key == "previous":
                sheet.write(i + 1, column_count, record[key])
            elif key == "next":
                sheet.write(i + 1, column_count + 1, record[key])
            else:
                if isinstance(record[key], list):
                    record[key] = ', '.join(map(str, record[key]))
                if len(str(record[key])) * 256 > sheet.col(column_count).width and len(str(record[key])) * 256 < 100 * 256:
                    sheet.col(j).width = len(str(record[key])) * 256
                sheet.write(i + 1, j, record[key])
                j += 1


def json_to_excel(input_json):
    data = input_json["instance"]

    excel_file = xlwt.Workbook(encoding="utf-8")

    print_sheet("events", data, excel_file)

    output = BytesIO()
    excel_file.save(output)
    output.seek(0)
    return output


def string_to_int(instance,element_type,field_name):
    for record in instance[element_type]:
        if record[field_name] != "" and record is not None:
            record[field_name] = int(record[field_name])
