import bson.json_util as json
import requests

cluster_address = 'http://master:6066'
create_job_endpoint_path = '/v1/submissions/create'


def is_list_of_strings(lst):
    return bool(lst) and isinstance(lst, list) and all(isinstance(elem, str) for elem in lst)


def send_submission_request(params):

    if not is_list_of_strings(params):
        raise TypeError("send_submission_request function must be applied to string")

    data = {'action': 'CreateSubmissionRequest',
            'appArgs': params,
            'appResource': 'file:/app/ClusterApp-1.0-SNAPSHOT.jar',
            "clientSparkVersion": "1.5.1",
            "environmentVariables": {
                "SPARK_ENV_LOADED": "1"},
            "mainClass": "pl.parota.cluster.Main",
            "sparkProperties": {
                "spark.jars": "file:/app/ClusterApp-1.0-SNAPSHOT.jar",
                "spark.driver.supervise": "false",
                "spark.app.name": "App",
                "spark.eventLog.enabled": "true",
                "spark.submit.deployMode": "cluster",
                "spark.master": "spark://master:6066"}
            }
    headers = {'Content-Type': 'application/json'}
    return requests.post(cluster_address + create_job_endpoint_path, data=json.dumps(data), headers=headers)

