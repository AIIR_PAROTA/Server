from flask import Blueprint
from backend import db, app
import bson.json_util as json
from bson.objectid import ObjectId
from flask.ext.api import status
from flask import Blueprint, request, abort, send_file
import requests
import string
import random
from ..util import excel_converter
from ..util.cluster_operations import send_submission_request


jobs = Blueprint('jobs', __name__, url_prefix='/jobs')
collection = db.jobs


@jobs.route('', methods=['GET'])
def get_all_jobs():
    return json.dumps(collection.find())


@jobs.route('/upload', methods=['POST'])
def upload_file():
    if not request.files:
        app.logger.error('Missing file contents')
        abort(status.HTTP_400_BAD_REQUEST)

    excel_json = excel_converter.excel_to_json(request.files['file'])

    json_in = {"instance": json.loads(excel_json), "progress": 0}
    job_id = collection.insert_one(json_in).inserted_id

    return json.dumps(job_id)


@jobs.route('', methods=['POST'])
def create_job():
    if not request.json:
        app.logger.error('Missing job json')
        abort(status.HTTP_400_BAD_REQUEST)
    job_json = request.json
    job_id = job_json['_id']
    job = collection.find_one({'_id': ObjectId(job_id)})
    job_json.update(job)
    job_id = collection.save(job_json)

    params = [app.config["DB_URL"], app.config["DB_NAME"], "jobs", str(job_id)]
    app.logger.info('job created, sending to Cluster...')
    try:
        r = send_submission_request(params)
    except (requests.exceptions.RequestException, TypeError) as e:
        app.logger.error('Sending failed: ', e)
        collection.delete_one({'_id': job_id})
        abort(status.HTTP_500_INTERNAL_SERVER_ERROR)
    r_json = r.json()

    collection.find_one_and_update({'_id': job_id}, {'$set': {'submissionId': r_json['submissionId']}})

    if r_json['success'] == 'false':
        app.logger.info('Cluster received message, but could not create job')
        return json.dumps({"job_id": job_id, "success": "false"}), status.HTTP_500_INTERNAL_SERVER_ERROR
    app.logger.info('Job successfully created')

    return json.dumps({"job_id": job_id, "success": "true"}), status.HTTP_201_CREATED


@jobs.route('/<job_id>', methods=['GET'])
def get_job(job_id):

    app.logger.info('Searching for a job')
    job = collection.find_one({'_id': ObjectId(job_id)})

    if 'submissionId' not in job:
        app.logger.error('submissionId undefined in found job')
        abort(status.HTTP_500_INTERNAL_SERVER_ERROR)
    url = 'http://master:6066/v1/submissions/status/'+job["submissionId"]
    app.logger.info(url)

    try:
        r = requests.get(url)
    except:
        app.logger.info('Sending failed')
        abort(status.HTTP_500_INTERNAL_SERVER_ERROR)

    r_json = r.json()
    r_json.update(job)
    return json.dumps(r_json)


@jobs.route('/user/<user_id>', methods=['GET'])
def get_user_job(user_id):
    return json.dumps(collection.find({'user_id': user_id}))


@jobs.route('/<job_id>/download', methods=['GET'])
def download_file(job_id):
    app.logger.info('Download request received')
    job = collection.find_one({'_id': ObjectId(job_id)})

    if 'progress' not in job:
        app.logger.error('"Progress" not found in job')
        abort(status.HTTP_500_INTERNAL_SERVER_ERROR)

    if job["progress"] < 100:
        app.logger.error('Job is not ready jet')
        abort(status.HTTP_400_BAD_REQUEST)

    app.logger.info('Parsing data do xls')
    file = excel_converter.json_to_excel(job)
    app.logger.info('Parsed, sending...')
    return send_file(file, attachment_filename=job["job_name"] + "_result.xls", as_attachment=True), status.HTTP_200_OK


@jobs.route('/<job_id>/progress', methods=['GET'])
def get_progress(job_id):
    app.logger.info('Progress request received')
    job = collection.find_one({'_id': ObjectId(job_id)})

    if 'progress' not in job:
        app.logger.error('"Progress" not found in job')
        abort(status.HTTP_500_INTERNAL_SERVER_ERROR)

    #temp solution -- delete when Cluster is ready (updates progress)
    a = job["progress"]

    return json.dumps(a)
