DEBUG = True
SESSION_COOKIE_HTTPONLY = False
SECRET_KEY = "secret"
EMAIL_CONFIRM_KEY = "secret"
BCRYPT_LOG_ROUNDS = 12
DB_URL = "mongodb://mongodb:27017/"
DB_NAME = "ParotaDB"
